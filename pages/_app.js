import '../assets/css/style.css'
import '../node_modules/bootstrap/dist/css/bootstrap.min.css'

const App = ({ Component, pageProps }) => <Component {...pageProps} />

export default App
